// app.controller.ts
import { Guid } from 'guid-typescript';
import { Request, Response } from 'express';
import { Controller, Inject, Get, Post, Req, Res, Body, HttpStatus, UsePipes, UseInterceptors } from '@nestjs/common';
import { BaseController } from './base.controller';
import { ZBClient } from 'zeebe-node';
import { CreateWorkflowInstanceResponse } from 'zeebe-node/interfaces';
import { ZEEBE_CONNECTION_PROVIDER, ZeebeWorker, ZeebeServer } from '@payk/nestjs-zeebe';
import { Consumer } from 'rabbitmq-provider/consumer';
import { AppService } from '../services/app.service';
import { RabbitmqPublisherService } from '../services/rabbitmq-publisher.service';
import { RabbitmqExchDirectPublisherService } from '../services/rabbitmq-exch-direct-publisher.service';
import { RabbitmqConsumerService } from '../services/rabbitmq-consumer.service';
import { RabbitmqChunkConsumerService } from '../services/rabbitmq-chunk-consumer.service';

class Message {
  constructor(public id: number, public text: string) { }
}

@Controller()
export class AppController extends BaseController {
  constructor(
    @Inject(ZEEBE_CONNECTION_PROVIDER) private zbClient: ZBClient,
    private zeebeServer: ZeebeServer,
    private appService: AppService,
    rabbitmqPublisherService: RabbitmqPublisherService, // publisher to submit result to gateway
    rabbitmqExchDirectPublisherService: RabbitmqExchDirectPublisherService, // exchange direct publisher
    rabbitmqConsumerService: RabbitmqConsumerService, // gateway consumer
    rabbitmqChunkConsumerService: RabbitmqChunkConsumerService, // chunks exchange direct consumer
  ) {
    super(rabbitmqPublisherService, rabbitmqExchDirectPublisherService, rabbitmqConsumerService, rabbitmqChunkConsumerService,
      (consumer, messages) => {
        const payloads = Consumer.getPayloads(messages);
        console.log(`@@@ send back events: ${JSON.stringify(messages)}`);
        payloads.forEach(item => this.sendResultToClient(item.sessionId, item.result));
      },
      events => {
        console.log(`*** events: ${JSON.stringify(events)}`);
      });
  }

  // Use the client to create a new workflow instance
  @Get('/1')
  async startWf1(@Req() req: Request, @Res() res: Response): Promise<CreateWorkflowInstanceResponse> {
    const sessionId = `${Guid.create()}`;
    const wfi = await this.zbClient.createWorkflowInstance('order-process-1', {
      sessionId,
      tracer: 'init',
    });

    console.log(`Workflow 1: ${JSON.stringify(wfi)}`);

    this.rabbitmqExchDirectPublisherService.publish(new Message(1, 'aa'));

    this.responses.set(sessionId, res);
    return wfi;
  }

  // Use the client to create a new workflow instance
  @Get('/2')
  async startWf2(@Req() req: Request, @Res() res: Response): Promise<CreateWorkflowInstanceResponse> {
    const sessionId = `${Guid.create()}`;
    const wfi = await this.zbClient.createWorkflowInstance('order-process-2', {
      sessionId,
      tracer: 'init',
    });

    console.log(`Workflow 2: ${JSON.stringify(wfi)}`);

    this.rabbitmqExchDirectPublisherService.publish(new Message(1, 'aa'));

    this.responses.set(sessionId, res);
    return wfi;
  }

  // ZeebeWorker 1
  // Subscribe to events of type 'task-1-1' and
  //   create a worker with the options as passed below (zeebe-node ZBWorkerOptions)
  @ZeebeWorker('task-1-1', { maxJobsToActivate: 10, timeout: 300 })
  task11(job, complete) { this.runTask(job, complete, this.appService.task1); }

  @ZeebeWorker('task-1-2')
  task12(job, complete) { this.runTask(job, complete, this.appService.task2, 3); }

  @ZeebeWorker('task-1-3')
  task13(job, complete) { this.runTask(job, complete, this.appService.task3, 1); }

  @ZeebeWorker('task-1-4')
  task14(job, complete) { this.runTask(job, complete, this.appService.task4, 1); }

  // ZeebeWorker 2
  @ZeebeWorker('task-2-1', { maxJobsToActivate: 10, timeout: 300 })
  task21(job, complete) { this.runTask(job, complete, this.appService.task1); }

  @ZeebeWorker('task-2-2')
  task22(job, complete) { this.runTask(job, complete, this.appService.task2, 4); }

  @ZeebeWorker('task-2-3')
  task23(job, complete) { this.runTask(job, complete, this.appService.task3, 2); }

  @ZeebeWorker('task-2-4')
  task24(job, complete) { this.runTask(job, complete, this.appService.task4, 2); }
}
