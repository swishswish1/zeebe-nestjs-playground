/* tslint:disable:curly */
// app.controller.ts
import { Response } from 'express';
import { HttpStatus } from '@nestjs/common';
import { RabbitmqPublisherService } from '../services/rabbitmq-publisher.service';
import { RabbitmqExchDirectPublisherService } from '../services/rabbitmq-exch-direct-publisher.service';
import { RabbitmqConsumerService } from '../services/rabbitmq-consumer.service';
import { RabbitmqChunkConsumerService } from '../services/rabbitmq-chunk-consumer.service';

export class BaseController {
  responses = new Map<string, Response>();

  constructor(
    protected readonly rabbitmqPublisherService: RabbitmqPublisherService, // publisher to submit result to gateway
    protected readonly rabbitmqExchDirectPublisherService: RabbitmqExchDirectPublisherService, // exchange direct publisher
    protected readonly rabbitmqConsumerService: RabbitmqConsumerService, // gateway consumer
    protected readonly rabbitmqChunkConsumerService: RabbitmqChunkConsumerService, // chunks exchange direct consumer
    fnConsume,
    fnChuncks,
  ) {
    // Gateway consumer
    this.rabbitmqConsumerService.createAndStartProcessing(fnConsume)
      .then(() => console.log('Gateway consumer created and started processing'));

    // Chunks exchange direct consumer
    this.rabbitmqChunkConsumerService.createAndStartChunksProcessing(fnChuncks)
      .then(() => console.log('Exchange direst consumer created and started chunks processing'));

    // Publisher to submit result to gateway
    this.rabbitmqPublisherService.create()
      .then(() => console.log('Gateway publisher created'));

    // Exchange direct publisher
    this.rabbitmqExchDirectPublisherService.create()
      .then(() => console.log('Exchange direst publisher created'));
  }

  protected sendResultToClient(sessionId: string, result: any) {
    const response = this.responses.get(sessionId);
    if (response) {
      response.status(HttpStatus.OK).send(`Workflow result -> ${JSON.stringify(result)}`);
      this.responses.delete(sessionId);
    }
  }

  protected sendResultToGateway = (sessionId: string, result: any) =>
    this.rabbitmqPublisherService.publish({ sessionId, result })

  protected runTask(job, complete, taskFn, args = null) {
    const result = taskFn(job, complete, args);
    if (result)
      this.sendResultToGateway(job.variables.sessionId, result);
  }
}
