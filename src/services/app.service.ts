import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  task1(job, complete) {
    console.log('task-1 -> Task variables', job.variables);

    // Task worker business logic
    const result = '1';

    const variableUpdate = {
      tracer: 'task-1',
      status: 'ok',
      result,
    };

    complete.success(variableUpdate);
  }

  task2(job, complete, nextTask) {
    console.log('task-2 -> Task variables', job.variables);

    // Task worker business logic
    const result = job.variables.result + '2';

    const variableUpdate = {
      tracer: 'task-2',
      status: 'ok',
      result,
      nextTask,
    };

    complete.success(variableUpdate);
  }

  task3(job, complete, wf): any {
    console.log('task-3 -> Task variables', job.variables);

    // Task worker business logic
    const result = `workflow: <b><i>${wf}</i></b>  ${job.variables.result}.3... ${job.variables.sessionId}`;

    const variableUpdate = {
      tracer: 'task-3',
      status: 'ok',
      result,
    };

    complete.success(variableUpdate);
    return result;
  }

  task4(job, complete, wf): any {
    console.log('task-4 -> Task variables', job.variables);

    // Task worker business logic
    const result = `workflow: <b><i>${wf}</i></b>  ${job.variables.result}.4... ${job.variables.sessionId}`;

    const variableUpdate = {
      tracer: 'task-4',
      status: 'ok',
      result,
    };

    complete.success(variableUpdate);
    return result;
  }
}
